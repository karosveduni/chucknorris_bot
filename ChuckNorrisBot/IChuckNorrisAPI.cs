﻿using Refit;

namespace ChuckNorrisBot
{
	public interface IChuckNorrisAPI
	{
		[Get("/jokes/random")]
		Task<ChuckNorrisData> GetChuckNorrisData(CancellationToken cancellationToken);

		[Get("/jokes/categories")]
		Task<List<string>> GetCategories(CancellationToken cancellationToken);

		[Get("/jokes/random?category={category}")]
		Task<ChuckNorrisData> GetChuckNorrisDataWithCategories(string category, CancellationToken cancellationToken);
	}
}
