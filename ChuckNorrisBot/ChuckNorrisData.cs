﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace ChuckNorrisBot
{
	public record ChuckNorrisData
	{
		private JsonSerializerOptions _options = new JsonSerializerOptions
		{
			WriteIndented = true,
		};

		[JsonPropertyName("icon_url")]
		public string? IconUrl { get; set; }
		public string? Id { get; set; }
		public string? Url { get; set; }
		public string? Value { get; set; }

		public string GetJsonString()
		{
			return JsonSerializer.Serialize(this, _options);
		}
	}
}
