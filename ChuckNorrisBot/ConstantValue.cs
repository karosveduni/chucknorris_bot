﻿namespace ChuckNorrisBot
{
	public static class ConstantValue
	{
		public const string BaseUrl = "https://api.chucknorris.io";
		public const string UserNameAuthor = "@c60a81cefc9bfea8e45857ddeb6c58d3";
		public const string ChooseButton = "Choose a button";
		public const string NoFact = "No fact";
		public const string Fact = "fact";
		public const string Categories = "categories";
		public const string Site = "site";
		public const string BotAuthor = "bot author";
		public const string Back = "BACK";
		public const string SiteUrl = "https://api.chucknorris.io/";
		public const string TokenFile = "Token.txt";
		public const string LogFile = "Log.txt";
	}
}
