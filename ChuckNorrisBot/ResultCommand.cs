﻿using Telegram.Bot.Types.ReplyMarkups;

namespace ChuckNorrisBot
{
	public record ResultCommand
	{
		public ChuckNorrisData? ChuckNorrisData { get; set; }
		public ReplyKeyboardMarkup? Buttons { get; set; }
	}
}
