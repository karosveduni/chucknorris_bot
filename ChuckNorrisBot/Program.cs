﻿using ChuckNorrisBot;

using Refit;

using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Serilog;
using Serilog.Core;

string Token = System.IO.File.ReadAllText(ConstantValue.TokenFile);

using Logger Log = new LoggerConfiguration()
#if DEBUG
	.WriteTo.Console()
#else
	.WriteTo.File(ConstantValue.LogFile)
#endif
	.CreateLogger();

IChuckNorrisAPI chuckNorrisAPI = RestService.For<IChuckNorrisAPI>(ConstantValue.BaseUrl);
ChuckNorrisData chuckNorrisData = null;

TelegramBotClientOptions options = new TelegramBotClientOptions(Token);

TelegramBotClient telegramBotClient = new TelegramBotClient(options);

ReplyKeyboardMarkup listButton = new ReplyKeyboardMarkup(new KeyboardButton[][]
{
	new KeyboardButton[]
	{
		new KeyboardButton(ConstantValue.Fact),
		new KeyboardButton(ConstantValue.Categories),
	},
	new KeyboardButton[]
	{
		new KeyboardButton(ConstantValue.Site),
		new KeyboardButton(ConstantValue.BotAuthor),
	}
});

ReplyKeyboardMarkup categoriesButton = new ReplyKeyboardMarkup(new KeyboardButton[][]
{
	new KeyboardButton[]
	{
		new KeyboardButton("animal"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("career"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("celebrity"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("dev"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("explicit"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("fashion"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("food"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("history"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("money"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("movie"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("music"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("political"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("religion"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("science"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("sport"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton("travel"),
	},
	new KeyboardButton[]
	{
		new KeyboardButton(ConstantValue.Back),
	},
});

ReceiverOptions receiverOptions = new ReceiverOptions
{
	AllowedUpdates = Array.Empty<UpdateType>(),
};

telegramBotClient.StartReceiving(OnMessageAsync, OnErrorAsync, receiverOptions, CancellationToken.None);

while (Console.ReadKey().Key != ConsoleKey.Enter) ;

async Task OnMessageAsync(ITelegramBotClient telegramBotClient, Update update, CancellationToken cancellationToken)
{
	if (update.Message is not { Text: not null })
		return;
	
	ReplyKeyboardMarkup buttons = null;
	string text = ConstantValue.NoFact;
	Log.Information(update.Message.Text);
	switch (update.Message.Text)
	{
		case ConstantValue.Fact:
			chuckNorrisData = await chuckNorrisAPI.GetChuckNorrisData(cancellationToken);
			text = chuckNorrisData.Value!;
			Log.Information(chuckNorrisData.GetJsonString());
			buttons = listButton;
			break;
		case ConstantValue.Categories:
			buttons = categoriesButton;
			text = ConstantValue.ChooseButton;
			break;
		case ConstantValue.Site:
			text = ConstantValue.SiteUrl;
			break;
		case ConstantValue.BotAuthor:
			text = ConstantValue.UserNameAuthor;
			buttons = listButton;
			break;
		default:
			KeyboardButton? result = categoriesButton.Keyboard.SelectMany(value => value).FirstOrDefault(s => s.Text.Equals(update.Message.Text, StringComparison.OrdinalIgnoreCase));

			if (result is not null)
			{
				if (result.Text == ConstantValue.Back)
				{
					text = ConstantValue.ChooseButton;
					buttons = listButton;
				}
				else
				{
					chuckNorrisData = await chuckNorrisAPI.GetChuckNorrisDataWithCategories(result.Text, cancellationToken);
					Log.Information(chuckNorrisData.GetJsonString());
					text = chuckNorrisData.Value;
					buttons = categoriesButton;
				}
			}
			else
			{
				text = ConstantValue.ChooseButton;
				buttons = listButton;
			}
			break;
	};

	_ = await telegramBotClient.SendTextMessageAsync(update.Message.Chat.Id, text, replyMarkup: buttons, cancellationToken: cancellationToken);
}

Task OnErrorAsync(ITelegramBotClient telegramBotClient, Exception exception, CancellationToken cancellationToken)
{
	Log.Error(exception, "БЛЯЯЯ БОТ УПАЛ");
	return Task.CompletedTask;
}